function [forwBackVel, leftRightVel, rotVel, finish] = ...
    solution2(pts, contacts, position, orientation, varargin)
% The control loop callback function - ..
% the solution for task 2

    % get the destination point
    if length(varargin) ~= 1,
        error(['Wrong number of additional ' ...
            'arguments: %d\n'], length(varargin));
    end 
    radius = varargin{1};

    % declare the persistent variable that 
    % keeps the state of the Finite
    % State Machine (FSM)
    persistent state;
    if isempty(state),
        % the initial state of the FSM is 'init'
        state = 'init';
    end

    % initialize the robot control variables 
    % (returned by this function)
    finish = false;
    forwBackVel = 0;
    leftRightVel = 0;
    rotVel = 0;

    % TODO: manage the states of FSM
    % Write your code here...
    
    u_max = 4;
    u_min = -4;
    gain_P = 25;
    tan_max_vel = 4;

    persistent initial_pos;
    persistent center;
    if strcmp(state, 'init')
        state = 'move';
        fprintf('changing FSM state to %s\n', state);
        initial_pos = position(1:2);
        center = initial_pos + [radius, 0];
    elseif strcmp(state, 'move')
        % calculate the differnce between center 
        % and current Position of the robot
        diff_from_center = center' - position(1:2)';

        %calculate the distance between the center 
        % and current Position of the robot
        new_Radius = norm(diff_from_center);

        %calculate the radial direction
        %for a unit vector
        rad_dir = diff_from_center / new_Radius;

        %calculate the tangengial direction perpendicular
        %to radial direction
        tan_dir = Rotation_matrix(-pi/2)*rad_dir;

        %Add proportional regulators with limited output
        radius_err =  radius- new_Radius;
        u_r = gain_P * radius_err;
        if u_r > u_max
            u_r = u_max;
        elseif u_r < u_min
            u_r = u_min;
        end

        Vel_glob = rad_dir * u_r + tan_dir * tan_max_vel;
        vel_local = Rotation_matrix(orientation(3))*Vel_glob;

        forwBackVel = vel_local(2);
        leftRightVel = vel_local(1);
        rotVel = 0;
    else
        error('Unknown state %s.\n', state);
    end
end

function R = Rotation_matrix(theta)
    R = [cos(theta), -sin(theta); 
        sin(theta), cos(theta)];
end
